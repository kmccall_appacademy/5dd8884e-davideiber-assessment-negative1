# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  missing_array = []
  full_array = (nums[0]..nums[-1]).to_a
  full_array.each do |num|
    if nums.include?(num)
      next
    else
      missing_array.push(num)
    end
  end
  missing_array
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  total = 0
  num_array = binary.to_s.reverse.chars
  num_array.each_with_index do |digit, i|
    total += digit.to_i * (2**i)
  end
  total
end

class Hash

  # Hash#select passes each k-value pair of a hash to the block (the proc
  # accepts two arguments: a k and a value). k-value pairs that return true
  # when passed to the block are added to a new hash. k-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

def my_select(&prc)
  selected = {}
  self.each do |k, v|
    selected[k] = v if prc.call(k, v)
  end
  selected
end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a k and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # k to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    result_hash = {}
    result_hash = self.clone
    hash.each do |k, v|
      result_hash[k] = v
    end

    if prc != nil
      merged_hash = {}
      result_hash.each do |k,v|
        value = nil
         if self.keys.include?(k)
           value = self[k]
         else
           value = 0
         end
         if self.keys.include?(k) && hash.keys.include?(k)
           merged_hash[k] = prc.call(k,value,v)
         else
           merged_hash[k] = v
         end
      end
      return merged_hash
    end
   result_hash
  end

end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  return 2 if n == 0
  return 1 if n == 1
  initial_array = [2, 1]
  if n > 0
    (n-1).times do
      initial_array.push(initial_array[-1] + initial_array[-2])
    end
    initial_array[n]
  else
    (n.abs).times do
      initial_array.unshift(initial_array[1] - initial_array[0])
    end
    initial_array[0]
  end
end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def substrings(str)
  substring_array = []
  i = 0
  while i < str.length
    j = 0
    while j < str.length
      substring_array.push(str[i..j])
      j += 1
    end
  i += 1
  end
  substring_array.uniq
end

def palindrome?(str)
  str == str.reverse
end

def longest_palindrome(string)
  longest_substring = substrings(string).select { |str| palindrome?(str) }.sort{ |x, y| x.length-y.length}.last
  if longest_substring.length < 3 || longest_substring == nil
    return false
  else
    return longest_substring.length
  end
end
